<?php

namespace Tests\Unit\Models;

use App\Models\Appointment;
use App\Models\Building;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class BuildingTest extends TestCase
{
    use WithFaker;

    private Building $building;

    protected function setUp(): void
    {
        parent::setUp();
        $this->building = Building::factory()->create();
    }

    public function test_we_have_a_building_in_database_through_factory()
    {
        $this->assertDatabaseCount(Building::class, 1);
        $this->assertDatabaseHas(Building::class, [
            'title' => $this->building->title,
            'description' => $this->building->description,
            'zip_code' => $this->building->zip_code,
            'latitude' => floatval($this->building->latitude),
            'longitude' => floatval($this->building->longitude),
            'address' => $this->building->address
        ]);
    }

    public function test_we_have_contact_in_db_through_building_factory()
    {
        $this->assertDatabaseCount(\App\Models\Contact::class, 1);
    }

    public function test_we_can_set_an_appointment_through_building_relation()
    {
        $user = \App\Models\User::factory()->create();
        $appointment = $this->building->appointments()->create([
            'contact_id' => $this->building->contact_id,
            'user_id' => $user->id,
            'title' => $this->faker->title,
            'description' => $this->faker->text,
            'zip_code' => $this->faker->postcode,
            'address' => $this->faker->address,
            'date' => $this->faker->dateTimeBetween('now', '+1 month')
        ]);

        $this->assertDatabaseCount(Appointment::class, 1);
        $this->assertDatabaseHas(Appointment::class, [
            'contact_id' => $this->building->contact_id,
            'user_id' => $user->id,
            'building_id' => $this->building->id,
            'title' => $appointment->title,
            'description' => $appointment->description,
            'zip_code' => $appointment->zip_code,
            'address' => $appointment->address,
            'date' => $appointment->date
        ]);
    }
}
