<?php

namespace Tests\Unit\Models;

use App\Models\Appointment;
use App\Models\Building;
use App\Models\Contact;
use App\Models\User;
use Tests\TestCase;

class AppointmentTest extends TestCase
{
    private Appointment $appointment;

    protected function setUp(): void
    {
        parent::setUp();
        $this->appointment = Appointment::factory()->create();
    }

    public function test_we_have_an_appointment_in_database_through_factory()
    {
        $this->assertDatabaseCount(Appointment::class, 1);
        $this->assertDatabaseHas(Appointment::class, [
            'id' => $this->appointment->id,
            'contact_id' => $this->appointment->contact_id,
            'user_id' => $this->appointment->user_id,
            'building_id' => $this->appointment->building_id,
            'title' => $this->appointment->title,
            'description' => $this->appointment->description,
            'zip_code' => $this->appointment->zip_code,
            'address' => $this->appointment->address,
            'date' => $this->appointment->date
        ]);
    }

    public function test_we_have_a_contact_in_database_through_appointment_factory()
    {
        $this->assertDatabaseHas(Contact::class, [
            'id' => $this->appointment->contact->id,
            'first_name' => $this->appointment->contact->first_name,
            'last_name' => $this->appointment->contact->last_name,
            'birthday' => $this->appointment->contact->birthday,
            'email' => $this->appointment->contact->email,
            'residence_building_id' => $this->appointment->contact->residence_building_id,
        ]);
    }

    public function test_we_have_a_user_in_database_through_appointment_factory()
    {
        $this->assertDatabaseHas(User::class, [
            'id' => $this->appointment->user->id,
            'first_name' => $this->appointment->user->first_name,
            'last_name' => $this->appointment->user->last_name,
            'email' => $this->appointment->user->email,
        ]);
    }

    public function test_we_have_a_building_in_database_through_appointment_factory()
    {
        $this->assertDatabaseHas(Building::class, [
            'id' => $this->appointment->building->id,
            'contact_id' => $this->appointment->building->contact_id,
            'title' => $this->appointment->building->title,
            'description' => $this->appointment->building->description,
            'zip_code' => $this->appointment->building->zip_code,
            'latitude' => $this->appointment->building->latitude,
            'longitude' => $this->appointment->building->longitude,
            'address' => $this->appointment->building->address,
        ]);
    }

}
