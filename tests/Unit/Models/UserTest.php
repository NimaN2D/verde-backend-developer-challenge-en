<?php

namespace Models;

use App\Models\Appointment;
use App\Models\PhoneNumber;
use App\Models\User;
use Tests\TestCase;

class UserTest extends TestCase
{
    private User $user;

    protected function setUp(): void
    {
        parent::setUp();
        $this->user = User::factory()->create();
    }

    public function test_we_have_a_user_in_database_through_factory()
    {
        $this->assertDatabaseCount(User::class,1);
    }

    public function test_create_a_phone_number_for_user_with_active_status_through_model_relation()
    {
        $this->user->phoneNumbers()->create([
            'phone_number' => '+905347769210'
        ]);
        $this->assertDatabaseCount(PhoneNumber::class,1);
        $this->assertDatabaseHas(PhoneNumber::class, [
            'owner_id' => $this->user->id,
            'owner_type' => $this->user->getMorphClass(),
            'phone_number' => '+905347769210',
            'is_active' => true
        ]);
    }

    public function test_create_a_phone_number_for_user_without_active_status_through_model_relation()
    {
        $this->user->phoneNumbers()->create([
            'phone_number' => '+905347769210',
            'is_active' => false
        ]);
        $this->assertDatabaseCount(PhoneNumber::class,1);
        $this->assertDatabaseHas(PhoneNumber::class, [
            'owner_id' => $this->user->id,
            'owner_type' => $this->user->getMorphClass(),
            'phone_number' => '+905347769210',
            'is_active' => false
        ]);
    }

    public function test_create_a_appointment_for_user_through_model_relation()
    {
        $appointment = Appointment::factory()->create([
            'user_id' => $this->user->id
        ]);
        $this->assertDatabaseCount(Appointment::class, 1);
        $this->assertDatabaseHas(Appointment::class, [
            'contact_id' => $appointment->contact_id,
            'user_id' => $this->user->id,
            'building_id' => $appointment->building_id,
            'title' => $appointment->title,
            'description' => $appointment->description,
            'zip_code' => $appointment->zip_code,
            'address' => $appointment->address,
            'date' => $appointment->date
        ]);
    }
}
