<?php

namespace Tests\Unit\Models;

use App\Models\Appointment;
use App\Models\Building;
use App\Models\Contact;
use App\Models\PhoneNumber;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ContactTest extends TestCase
{
    use WithFaker;

    private Contact $contact;

    protected function setUp(): void
    {
        parent::setUp();
        $this->contact = Contact::factory()->create();
    }

    public function test_we_have_a_contact_in_database_through_factory()
    {
        $this->assertDatabaseCount(Contact::class, 1);
        $this->assertDatabaseHas(Contact::class, [
            'id' => $this->contact->id,
            'first_name' => $this->contact->first_name,
            'last_name' => $this->contact->last_name,
            'birthday' => $this->contact->birthday,
            'email' => $this->contact->email,
            'residence_building_id' => $this->contact->residence_building_id
        ]);
    }

    public function test_add_phone_number_for_contact_through_relation()
    {
        $phoneNumber = $this->contact->phoneNumbers()->create([
            'phone_number' => $this->faker->phoneNumber,
            'is_active' => false
        ]);

        $this->assertDatabaseCount(PhoneNumber::class, 1);
        $this->assertDatabaseHas(PhoneNumber::class, [
            'id' => $phoneNumber->id,
            'owner_id' => $this->contact->id,
            'owner_type' => $this->contact->getMorphClass(),
            'phone_number' => $phoneNumber->phone_number,
            'is_active' => $phoneNumber->is_active
        ]);
    }

    public function test_add_building_for_contact_through_relation()
    {
        $building = $this->contact->buildings()->create([
            'contact_id' => $this->contact->id,
            'title' => $this->faker->title,
            'description' => $this->faker->text,
            'zip_code' => $this->faker->postcode,
            'latitude' => $this->faker->latitude,
            'longitude' => $this->faker->longitude,
            'address' => $this->faker->address
        ]);

        $building = $building->refresh();

        $this->assertDatabaseCount(Building::class, 1);
        $this->assertDatabaseHas(Building::class, [
            'id' => $building->id,
            'contact_id' => $this->contact->id,
            'title' => $building->title,
            'description' => $building->description,
            'zip_code' => $building->zip_code,
            'latitude' => floatval($building->latitude),
            'longitude' => floatval($building->longitude),
            'address' => $building->address
        ]);
    }

    public function test_add_residence_building_for_contact_through_relation()
    {
        $building = Building::factory()->create([
            'contact_id' => $this->contact->id
        ]);
        $this->contact->residenceBuilding()->associate($building);

        $this->contact->refresh();

        $this->assertDatabaseCount(Building::class, 1);
        $this->assertDatabaseHas(Building::class, [
            'id' => $building->id,
            'contact_id' => $this->contact->id,
            'title' => $building->title,
            'description' => $building->description,
            'zip_code' => $building->zip_code,
            'latitude' => floatval($building->latitude),
            'longitude' => floatval($building->longitude),
            'address' => $building->address
        ]);

        $this->assertDatabaseHas(Contact::class, [
            'id' => $this->contact->id,
            'first_name' => $this->contact->first_name,
            'last_name' => $this->contact->last_name,
            'birthday' => $this->contact->birthday,
            'email' => $this->contact->email,
            'residence_building_id' => $this->contact->residence_building_id
        ]);
    }

    public function test_add_appointment_for_contact_through_relation()
    {
        $user = \App\Models\User::factory()->create();
        $building = Building::factory()->create([
            'contact_id' => $this->contact->id
        ]);

        $appointment = $this->contact->appointments()->create([
            'user_id' => $user->id,
            'building_id' => $building->id,
            'title' => $this->faker->title,
            'description' => $this->faker->text,
            'zip_code' => null,
            'address' => null,
            'date' => now()->addHour(5)->toDateTime()
        ]);

        $this->assertDatabaseCount(Appointment::class, 1);
        $this->assertDatabaseHas(Appointment::class, [
            'id' => $appointment->id,
            'contact_id' => $this->contact->id,
            'useR_id' => $user->id,
            'building_id' => $building->id,
            'title' => $appointment->title,
            'description' => $appointment->description,
            'zip_code' => null,
            'address' => null,
            'date' => $appointment->date
        ]);
    }

}
