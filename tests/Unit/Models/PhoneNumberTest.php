<?php

namespace Tests\Unit\Models;

use App\Models\Contact;
use App\Models\PhoneNumber;
use App\Models\User;
use Tests\TestCase;

class PhoneNumberTest extends TestCase
{
    private PhoneNumber $phoneNumber;

    protected function setUp(): void
    {
        parent::setUp();
        $this->phoneNumber = PhoneNumber::factory()->create();
    }

    public function test_add_phone_number_through_factory()
    {
        $this->assertDatabaseCount(PhoneNumber::class, 1);
        $this->assertDatabaseHas(PhoneNumber::class, [
            'id' => $this->phoneNumber->id,
            'owner_id' => $this->phoneNumber->owner_id,
            'owner_type' => $this->phoneNumber->owner_type,
            'phone_number' => $this->phoneNumber->phone_number
        ]);
    }

    public function test_to_get_owner_through_relation()
    {
        $owner = $this->phoneNumber->owner;

        if ($owner instanceof User) {
            $this->assertDatabaseCount(User::class, 1);

            /**@var User $owner*/
            $this->assertDatabaseHas(User::class, [
                'id' => $owner->id,
                'first_name' => $owner->first_name,
                'last_name' => $owner->last_name,
                'email' => $owner->email
            ]);
        }

        if ($owner instanceof Contact) {
            $this->assertDatabaseCount(Contact::class, 1);

            /**@var Contact $owner*/
            $this->assertDatabaseHas(Contact::class, [
                'id' => $owner->id,
                'first_name' => $owner->first_name,
                'last_name' => $owner->last_name,
                'birthday' => $owner->birthday,
                'email' => $owner->email,
                'residence_building_id' => $owner->residence_building_id
            ]);
        }
    }
}
