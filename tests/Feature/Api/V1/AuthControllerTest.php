<?php

namespace Tests\Feature\Api\V1;


use App\Mail\V1\User\UserRegistrationConfirmationEmail;
use App\Models\User;
use Illuminate\Support\Facades\Mail;
use Tests\TestCase;

class AuthControllerTest extends TestCase
{
    public function test_register_user_with_empty_first_name_input_failed()
    {
        $requestBody = [
            'last_name' => 'Nouri',
            'email' => 'nima.nouri.d@gmail.com',
            'password' => '123456Aa@!',
            'password_confirmation' => '123456Aa@!',
        ];

        $response = $this->postJson(route('api.v1.register'), $requestBody);
        $response->assertStatus(422);
        $response->assertJsonStructure([
            'message',
            'errors' => [
                'first_name'
            ]
        ]);
    }

    public function test_register_user_with_empty_last_name_input_failed()
    {
        $requestBody = [
            'first_name' => 'Nima',
            'email' => 'nima.nouri.d@gmail.com',
            'password' => '123456Aa@!',
            'password_confirmation' => '123456Aa@!',
        ];

        $response = $this->postJson(route('api.v1.register'), $requestBody);
        $response->assertStatus(422);
        $response->assertJsonStructure([
            'message',
            'errors' => [
                'last_name'
            ]
        ]);
    }

    public function test_register_user_with_empty_email_input_failed()
    {
        $requestBody = [
            'first_name' => 'Nima',
            'last_name' => 'Nouri Delouei',
            'password' => '123456Aa@!',
            'password_confirmation' => '123456Aa@!',
        ];

        $response = $this->postJson(route('api.v1.register'), $requestBody);
        $response->assertStatus(422);
        $response->assertJsonStructure([
            'message',
            'errors' => [
                'email'
            ]
        ]);
    }

    public function test_register_user_with_invalid_email_failed()
    {
        $requestBody = [
            'first_name' => 'Nima',
            'last_name' => 'Nouri Delouei',
            'email' => 'invalid_email_goes_here',
            'password' => '123456Aa@!',
            'password_confirmation' => '123456Aa@!',
        ];

        $response = $this->postJson(route('api.v1.register'), $requestBody);
        $response->assertStatus(422);
        $response->assertJsonStructure([
            'message',
            'errors' => [
                'email'
            ]
        ]);
    }

    public function test_register_user_with_wrong_email_domain_failed()
    {
        $requestBody = [
            'first_name' => 'Nima',
            'last_name' => 'Nouri Delouei',
            'email' => 'nima.nouri.d@wrong-domain-address-form-email-column.com',
            'password' => '123456Aa@!',
            'password_confirmation' => '123456Aa@!',
        ];

        $response = $this->postJson(route('api.v1.register'), $requestBody);
        $response->assertStatus(422);
        $response->assertJsonStructure([
            'message',
            'errors' => [
                'email'
            ]
        ]);
    }

    public function test_register_user_without_password_input_failed()
    {
        $requestBody = [
            'first_name' => 'Nima',
            'last_name' => 'Nouri Delouei',
            'email' => 'nima.nouri.d@gmail.com',
        ];

        $response = $this->postJson(route('api.v1.register'), $requestBody);
        $response->assertStatus(422);
        $response->assertJsonStructure([
            'message',
            'errors' => [
                'password'
            ]
        ]);
    }

    public function test_register_user_without_password_confirmation_input_failed()
    {
        $requestBody = [
            'first_name' => 'Nima',
            'last_name' => 'Nouri Delouei',
            'email' => 'nima.nouri.d@gmail.com',
            'password' => '123456Aa@!',
        ];

        $response = $this->postJson(route('api.v1.register'), $requestBody);
        $response->assertStatus(422);
        $response->assertJsonStructure([
            'message',
            'errors' => [
                'password'
            ]
        ]);
    }

    public function test_register_user_with_wrong_password_confirmation_input_failed()
    {
        $requestBody = [
            'first_name' => 'Nima',
            'last_name' => 'Nouri Delouei',
            'email' => 'nima.nouri.d@gmail.com',
            'password' => '123456Aa@!',
            'password_confirmation' => '123456Aa@!_',
        ];

        $response = $this->postJson(route('api.v1.register'), $requestBody);
        $response->assertStatus(422);
        $response->assertJsonStructure([
            'message',
            'errors' => [
                'password'
            ]
        ]);
    }

    public function test_register_user_with_invalid_password_failed()
    {
        $requestBody = [
            'first_name' => 'Nima',
            'last_name' => 'Nouri Delouei',
            'email' => 'nima.nouri.d@gmail.com',
            'password' => '123',
            'password_confirmation' => '123',
        ];

        $response = $this->postJson(route('api.v1.register'), $requestBody);
        $response->assertStatus(422);
        $response->assertJsonStructure([
            'message',
            'errors' => [
                'password'
            ]
        ]);
    }

    public function test_register_user_successful()
    {
        $requestBody = [
            'first_name' => 'Nima',
            'last_name' => 'Nouri Delouei',
            'email' => 'nima.nouri.d@gmail.com',
            'password' => '123456@2!',
            'password_confirmation' => '123456@2!',
        ];

        Mail::fake();
        $response = $this->postJson(route('api.v1.register'), $requestBody);
        $response->assertSuccessful();
        $this->assertDatabaseCount(User::class, 1);
        $this->assertDatabaseHas(User::class, [
            'first_name' => 'Nima',
            'last_name' => 'Nouri Delouei',
            'email' => 'nima.nouri.d@gmail.com',
        ]);
        Mail::assertSent(UserRegistrationConfirmationEmail::class);

    }

    public function test_login_user_with_wrong_credentials_failed()
    {
        User::factory()->create([
            'email' => 'nima.nouri.d@gmail.com',
            'password' => 12345678
        ]);

        $credentials = [
            'email' => 'nima.nouri.d@gmail.com',
            'password' => 1234567
        ];

        $response = $this->postJson(route('api.v1.login'), $credentials);
        $response->assertStatus(422);
        $response->assertJsonStructure([
            'status',
            'success',
            'error' => [
                'code',
                'message',
            ],
        ]);
    }

    public function test_login_user_successful()
    {
        $user = User::factory()->create([
            'email' => 'nima.nouri.d@gmail.com',
            'password' => 12345678
        ]);

        $credentials = [
            'email' => 'nima.nouri.d@gmail.com',
            'password' => 12345678
        ];

        $response = $this->postJson(route('api.v1.login'), $credentials);
        $response->assertSuccessful();
        $response->assertJsonStructure([
            'status',
            'success',
            'data' => [
                'user',
                'access_token',
                'token_type'
            ]
        ]);
        $this->assertAuthenticatedAs($user);
    }
}
