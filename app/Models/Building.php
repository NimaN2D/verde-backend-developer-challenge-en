<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Building
 *
 * @property-read integer $id
 * @property-read integer $contact_id
 * @property-read string $title
 * @property-read string $description
 * @property-read string $zip_code
 * @property-read float $latitude
 * @property-read float $longitude
 * @property-read string $address
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Appointment[] $appointments
 * @property-read int|null $appointments_count
 * @property-read \App\Models\Contact|null $contact
 * @method static \Database\Factories\BuildingFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|Building newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Building newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Building query()
 * @mixin \Eloquent
 */
class Building extends Model
{
    use HasFactory;

    protected $fillable = [
        'contact_id',
        'title',
        'description',
        'zip_code',
        'latitude',
        'longitude',
        'address'
    ];

    protected $casts = [
        'contact_id' => 'int',
        'id' => 'int',
        'latitude' => 'decimal:8',
        'longitude' => 'decimal:8',
    ];

    public function contact(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Contact::class);
    }

    public function appointments(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Appointment::class);
    }
}
