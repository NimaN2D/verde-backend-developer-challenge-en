<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Appointment
 *
 * @property-read integer $id
 * @property-read integer $contact_id
 * @property-read integer $user_id
 * @property-read integer $building_id
 * @property-read string $title
 * @property-read string $description
 * @property-read string $zip_code
 * @property-read string $address
 * @property-read \DateTime|Carbon $date
 * @property-read \App\Models\Building|null $building
 * @property-read \App\Models\Contact|null $contact
 * @property-read \App\Models\User|null $user
 * @method static \Database\Factories\AppointmentFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|Appointment newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Appointment newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Appointment query()
 * @mixin \Eloquent
 */
class Appointment extends Model
{
    use HasFactory;

    protected $fillable = [
        'contact_id',
        'user_id',
        'building_id',
        'title',
        'description',
        'zip_code',
        'address',
        'date'
    ];

    protected $casts = [
        'contact_id' => 'int',
        'user_id' => 'int',
        'building_id' => 'int',
        'date' => 'datetime'
    ];

    public function contact(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Contact::class);
    }

    public function user(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function building(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Building::class);
    }
}
