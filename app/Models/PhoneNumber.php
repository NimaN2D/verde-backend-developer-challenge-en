<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\PhoneNumber
 *
 * @property-read integer $id
 * @property-read integer $owner_id
 * @property-read string $owner_type
 * @property-read string $phone_number
 * @property-read boolean $is_active
 * @property-read Model|\Eloquent $owner
 * @method static \Database\Factories\PhoneNumberFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|PhoneNumber newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PhoneNumber newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PhoneNumber query()
 * @mixin \Eloquent
 */
class PhoneNumber extends Model
{
    use HasFactory;

    protected $fillable = [
        'owner_id',
        'owner_type',
        'phone_number',
        'is_active'
    ];

    protected $casts = [
        'is_active' => 'boolean'
    ];

    protected $hidden = [
        'owner_id',
        'owner_type'
    ];

    public function owner(): \Illuminate\Database\Eloquent\Relations\MorphTo
    {
        return $this->morphTo();
    }
}
