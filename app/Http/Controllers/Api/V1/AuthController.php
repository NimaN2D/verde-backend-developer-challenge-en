<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\V1\Auth\LoginRequest;
use App\Http\Requests\Api\V1\Auth\RegisterRequest;
use App\Models\User;
use App\Repositories\UserRepository;
use Illuminate\Http\JsonResponse;

class AuthController extends Controller
{
    private UserRepository $repository;
    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }


    /**
     * Register New User
     * @group Auth
     * @unauthenticated
     * @param RegisterRequest $request
     * @return JsonResponse
     */
    public function register(RegisterRequest $request): JsonResponse
    {
        $user = $this->repository->create($request->all());
        return responder()->success($user)->respond();
    }

    /**
     * Login User
     * @group Auth
     * @unauthenticated
     * @param LoginRequest $request
     * @return JsonResponse
     */
    public function login(LoginRequest $request): JsonResponse
    {
        if (!\Auth::attempt($request->only(['email', 'password']))) {
            return responder()->error(422, 'Invalid credentials.')->respond(422);
        }

        $user = User::query()->whereEmail($request->get('email'))->first();

        return responder()->success([
            'user' => $user,
            'access_token' => $user->createToken(config('APP_NAME', 'Verde'), []),
            'token_type' => 'Bearer'
        ])->respond();
    }
}
