<?php

namespace App\Providers;

use App\Events\V1\User\UserRegisteredEvent;
use App\Listeners\V1\User\UserRegisteredListener;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{

    protected $listen = [
        UserRegisteredEvent::class => [
            UserRegisteredListener::class,
        ],
    ];

    protected $observers = [
        \App\Models\User::class => \App\Observers\V1\UserObserver::class
    ];

    public function boot(): void
    {
        //
    }

    public function shouldDiscoverEvents(): bool
    {
        return false;
    }
}
