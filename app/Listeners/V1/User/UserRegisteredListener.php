<?php

namespace App\Listeners\V1\User;

use App\Events\V1\User\UserRegisteredEvent;
use App\Mail\V1\User\UserRegistrationConfirmationEmail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

class UserRegisteredListener implements ShouldQueue
{
    public function __construct(UserRegisteredEvent $event)
    {
        //
    }

    public function handle($event)
    {
        Mail::to($event->user)->send(new UserRegistrationConfirmationEmail($event->user));
    }
}
