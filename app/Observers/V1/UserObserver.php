<?php

namespace App\Observers\V1;

use App\Events\V1\User\UserRegisteredEvent;
use App\Models\User;

class UserObserver
{

    public bool $afterCommit = true;

    public function created(User $user): void
    {
        UserRegisteredEvent::dispatch($user);
    }

}
