<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use App\Models\Appointment;

class AppointmentRepositoryEloquent extends BaseRepository implements AppointmentRepository
{

    public function model(): string
    {
        return Appointment::class;
    }

}
