<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\UserRepository;
use App\Models\User;
use App\Validators\UserValidator;

class UserRepositoryEloquent extends BaseRepository implements UserRepository
{

    public function model(): string
    {
        return User::class;
    }

}
