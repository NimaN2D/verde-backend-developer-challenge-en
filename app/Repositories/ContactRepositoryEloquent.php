<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use App\Models\Contact;

class ContactRepositoryEloquent extends BaseRepository implements ContactRepository
{

    public function model(): string
    {
        return Contact::class;
    }

}
