<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use App\Models\Building;

class BuildingRepositoryEloquent extends BaseRepository implements BuildingRepository
{

    public function model(): string
    {
        return Building::class;
    }

}
