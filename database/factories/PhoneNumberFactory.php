<?php

namespace Database\Factories;

use App\Models\Contact;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\=PhoneNumber>
 */
class PhoneNumberFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        $owner = app($this->owner())->factory()->create();

        return [
            'owner_id' => $owner->id,
            'owner_type' => $owner->getMorphClass(),
            'phone_number' => $this->faker->e164PhoneNumber(),
            'is_active' => $this->faker->boolean
        ];
    }

    private function owner()
    {
        return $this->faker->randomElement([
            Contact::class,
            User::class
        ]);
    }
}
