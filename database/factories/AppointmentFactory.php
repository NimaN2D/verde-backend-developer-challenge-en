<?php

namespace Database\Factories;

use App\Models\Building;
use App\Models\Contact;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Appointment>
 */
class AppointmentFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'contact_id' => Contact::factory()->create(),
            'user_id' => User::factory()->create(),
            'building_id' => Building::factory()->create(),
            'title' => $this->faker->title,
            'description' => $this->faker->text,
            'zip_code' => $this->faker->postcode,
            'address' => $this->faker->address,
            'date' => $this->faker->dateTimeBetween('now','+1 month')
        ];
    }
}
